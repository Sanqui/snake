#gawk sort order
export LC_CTYPE=C

.SUFFIXES: .asm .o .gbc

all: game.gbc

game.o: game.asm
	rgbasm -o game.o game.asm

game.gbc: game.o
	rgblink -o $@ $<
	rgbfix -jv -i XXXX -k XX -l 0x33 -m 0x01 -p 0 -r 0 -t SNAKE $@

clean:
	rm -f game.o game.gbc
