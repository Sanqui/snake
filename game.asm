INCLUDE "constants.asm"

; rst vectors go unused
SECTION "rst00",HOME[0]
    ret

SECTION "rst08",HOME[8]
    ret

SECTION "rst10",HOME[$10]
    ret

SECTION "rst18",HOME[$18]
    ret

SECTION "rst20",HOME[$20]
    ret

SECTION "rst30",HOME[$30]
    ret

SECTION "rst38",HOME[$38]
    ret

SECTION "vblank",HOME[$40]
	jp VBlankHandler
SECTION "lcdc",HOME[$48]
	reti
SECTION "timer",HOME[$50]
	reti
SECTION "serial",HOME[$58]
	reti
SECTION "joypad",HOME[$60]
	reti

SECTION "bank0",HOME[$61]

SECTION "romheader",HOME[$100]
    nop
    jp Start

Section "start",HOME[$150]

VBlankHandler:
    push af
    push bc
    push de
    push hl
    call CopyTilemap
    call ReadJoypadRegister
    ld hl, H_TIMER
    inc [hl]
    ld a, [rDIV]
    ld b, a
    call GetRNG
    pop hl
    pop de
    pop bc
    pop af
    reti

CopyTilemap: ; We can copy just 8 lines per vblank.
; Contains an unrolled loop for speed.
    ;ld de, $9800
    ;ld hl, W_MAP
    ld hl, H_VCOPY_D
    ld a, [hli]
    ld d, a
    ld a, [hli]
    ld e, a
    ld a, [hli]
    ld c, a
    ld a, [hl]
    ld l, a
    ld h, c
    ld a, [H_VCOPY_ROWS]
    ld c, a
.row

    dec c
    jr z, .done

    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    ld a, [hli]
    ld [de], a
    inc de
    
    ld a, e
    add $c
    ld e, a
    jr nc, .row
;carry
    inc d
    jr .row
.done
    ld a, [H_VCOPY_TIMES]
    inc a
    ld [H_VCOPY_TIMES], a
    cp a, $03
    jr z, .reset
    cp a, $02
    jr nz, .eightrows
    ; only 5 rows left
    ld a, $5
    ld [H_VCOPY_ROWS], a

.eightrows
    ld a, d
    ld [H_VCOPY_D], a
    ld a, e
    ld [H_VCOPY_E], a
    ld a, h
    ld [H_VCOPY_H], a
    ld a, l
    ld [H_VCOPY_L], a
    ret
.reset
    ld a, $98
    ld [H_VCOPY_D], a
    xor a
    ld [H_VCOPY_E], a
    ld a, $C0
    ld [H_VCOPY_H], a
    xor a
    ld [H_VCOPY_L], a
    ld [H_VCOPY_TIMES], a
    ld a, $8
    ld [H_VCOPY_ROWS], a
    ret

DisableLCD: ; $0061
	xor a
	ld [$ff0f],a
	ld a,[$ffff]
	ld b,a
	res 0,a
	ld [$ffff],a
.waitVBlank
	ld a,[$ff44]
	cp a,$91
	jr nz,.waitVBlank
	ld a,[$ff40]
	and a,$7f	; res 7,a
	ld [$ff40],a
	ld a,b
	ld [$ffff],a
	ret

EnableLCD:
	ld a,[$ff40]
	set 7,a
	ld [$ff40],a
	ret

CopyData:
; copy bc bytes of data from hl to de
	ld a,[hli]
	ld [de],a
	inc de
	dec bc
	ld a,c
	or b
	jr nz,CopyData
	ret

CopyDataFF:
; copy data from hl to de ending with $ff (inclusive)
	ld a,[hli]
	ld [de],a
	inc de
	inc a
	ret z
	jr CopyDataFF

WriteDataInc:
; write data in hl increasing a until b.
.loop
    ld [hli], a
    inc a
    cp a, b
    jr nz, .loop
    ret

; copypasta:
; this function directly reads the joypad I/O register
; it reads many times in order to give the joypad a chance to stabilize
; it saves a result in [$fff8] in the following format
; (set bit indicates pressed button)
; bit 0 - A button
; bit 1 - B button
; bit 2 - Select button
; bit 3 - Start button
; bit 4 - Right
; bit 5 - Left
; bit 6 - Up
; bit 7 - Down
ReadJoypadRegister: ; 15F
    ld a, [H_JOY]
    ld [H_JOYOLD], a
	ld a,%00100000 ; select direction keys
	ld c,$00
	ld [rJOYP],a
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	cpl ; complement the result so that a set bit indicates a pressed key
	and a,%00001111
	swap a ; put direction keys in upper nibble
	ld b,a
	ld a,%00010000 ; select button keys
	ld [rJOYP],a
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	ld a,[rJOYP]
	cpl ; complement the result so that a set bit indicates a pressed key
	and a,%00001111
	or b ; put button keys in lower nibble
	ld [$fff8],a ; save joypad state
	ld a,%00110000 ; unselect all keys
	ld [rJOYP],a
	
	ld a, [H_JOY]
	ld b, a
	ld a, [H_JOYOLD]
	xor $ff
	and b
	ld [H_JOYNEW], a
	ret

GetRNG:
    ld a, [H_RNG1]
    xor b
    ld [H_RNG1], a
    ret

WaitForKey:
.loop
    halt
    ld a, [H_JOYNEW]
    and a, %00001001 ; A or START
    jr z, .loop
    ret

Start:
    di
    ld sp, $dffe
    
    ; palettes
    ld a, %11100100
    ld [rBGP], a
    ld [rOBP0], a
    
    ld a, 0
    ld [rSCX], a
    ld [rSCY], a
    
    ld a, %11000001
    ld [rLCDC], a
    
    ei
    
    call DisableLCD
    
    ld hl, $C000
.loop
    ld a, 0
    ld [hli], a
    ld a, h
    cp $e0
    jr nz, .loop

    ld hl, $ff80
.loop2
    ld a, 0
    ld [hli], a
    ld a, h
    cp $00
    jr nz, .loop2
    
    ld a, $98
    ld [H_VCOPY_D], a
    ld a, $C0
    ld [H_VCOPY_H], a
    ld a, $8
    ld [H_VCOPY_ROWS], a
    
    ld hl, Tiles
    ld de, $9000
    ld bc, TilesEnd-Tiles
    call CopyData
        
    ld hl, WelcomeString
    decoord 2, 2
    call WriteString
    
    ld hl, SnakeString
    decoord 3, 13
    call WriteString

    ld hl, VersionString
    decoord 6,4
    call WriteString
    
    ld hl, BySankyString
    decoord 7,6
    call WriteString
    
    ld hl, PressStartString
    decoord 9,4
    call WriteString
    
    hlcoord 12,4
    ld a, $60
    ld b, $6b
    call WriteDataInc
    hlcoord 13,4
    ld a, $70
    ld b, $7b
    call WriteDataInc
    
    call EnableLCD
    xor a
    ld [$ffff], a
    ld a, %00000001
    ld [$ffff], a
    ei
    
.titlewait
    halt
    ld a, [H_TIMER]
    cp $12
    jr nz, .keycheck
    xor a
    ld [H_TIMER], a
    ld a, [H_TITLEANIM]
    and a
    jr nz, .anim1
.anim0
    ld a, $60
    hlcoord 12,4
    ld [hli], a
    inc a
    ld [hl], a
    ld a, $70
    hlcoord 13,4
    ld [hli], a
    inc a
    ld [hl], a
    xor a
    inc a
    ld [H_TITLEANIM], a
    jr .keycheck
.anim1
    ld a, $6b
    hlcoord 12,4
    ld [hli], a
    inc a
    ld [hl], a
    ld a, $7b
    hlcoord 13,4
    ld [hli], a
    inc a
    ld [hl], a
    xor a
    ld [H_TITLEANIM], a
.keycheck
    ld a, [H_JOYNEW]
    and a, %00001001 ; A or START
    jr z, .titlewait
    
.begin
    call ClearScreen
    
    xor a
    ld [H_TIMER], a
    
    call SnakeInit

    call WaitForKey
    jr .begin

WelcomeString:
    db "WELCOME TO SNAKE@"
    ;db "WELCOME TO ", $5b, $5c, $5d, $5e, "@"

SnakeString:
    db "@";db "SNAKE@"

VersionString:
    db "VERSION THREE@"
BySankyString:
    db "BY SANKY@"

PressStartString:
    db "PRESS START@"
    
ScoreString:
    db "SCORE @"

GameOverString:
    db "GAME OVER@"
    
StartAgainString:
    db "START TO TRY AGAIN@"

PauseString:
    db "PAUSE@"

WriteString:
.loop
    ld a, [hli]
    cp a, "@"
    jr z, .done
    cp a, " "
    jr nz, .nospace
    ld a, 0
.nospace
    ld [de], a
    inc de
    jr .loop
.done
    ret
    
WriteNumber: ; writes number (less than 160) to de
    cp a, 159
    jr c, .write
    ld a, 159
.write
    push af
    ld b, 10
    call DivB
    add a, $30
    ld [de], a
    inc de
    pop af
    call ModuloB
    add a, $30
    ld [de], a
    ret

ClearScreen:
    ld hl, $c000
.loop
    xor a
    ld [hli], a
    ld a, h
    cp $c1
    jr nz, .loop
    ld a, l
    cp $68
    jr nz, .loop
    ret
    

GetTileAddr: ; bc = xy
    inc c
    ld a, c
    ld [H_TMPY], a
    ld hl, W_MAP
    ld e, $14
.loop
    dec c
    jr z, .end
    ld a, l
    add e
    ld l, a
    jr nc, .loop
    inc h
    jr .loop
.end
    ld a, l
    add b
    ld l, a
    jr nc, .nc
    inc h
.nc
    ld a, [H_TMPY]
    ld c, a
    ret

GetTileAt:
    call GetTileAddr
    ld a, [hl]
    ret

DrawHead:
    ld a, [H_PY]
    ld c, a
    ld a, [H_PX]
    ld b, a
    call GetTileAddr
    ld a, [H_PDIR]
    add $20
    ld [hl], a
    ret

DrawFood:
    ld a, [H_FY]
    ld c, a
    ld a, [H_FX]
    ld b, a
    call GetTileAddr
    ld a, [H_FFRAME]
    and a
    jr nz, .frame1
    inc a
    ld [H_FFRAME], a
    ld a, $3
    jr .frame
.frame1
    ld a, $2
.frame
    ld [hl], a
    ret
    
DrawTail:
    ld hl, H_FIRST
    ld [hl], $ff
    ld hl, W_TAIL
.loop
    ld a, [hli] ; x
    cp $ff
    ret z
    ld b, a
    ld a, [hli] ; y
    ld c, a
    push hl
    call GetTileAddr
    push hl
    pop de
    pop hl
    ld a, [hli] ; direction
    push hl
    ld hl, H_FIRST
    inc [hl]
    pop hl
    jr nz, .notfirst
    ; first part of the tail - the ending
    and %11110000
    swap a
    add a, $18
    jr .write
.notfirst
    and %00001111 ; keep low nybble, which is the tile
    add a, $10
.write
    ld [de], a
    jr .loop
    
PopTail:
    ld hl, W_TAIL+3
    ld de, W_TAIL
    call CopyDataFF
    ret

AppendTail:
    ld hl, W_TAIL
.loop
    ld a, [hli]
    cp a, $ff
    jr nz, .loop
    dec hl
    ld a, [H_PX]
    ld [hli], a
    ld a, [H_PY]
    ld [hli], a
    ld a, [H_PLASTDIR]
    and a
    jr z, .down
    dec a
    jr z, .up
    dec a
    jr z, .left
    jr .right
.down
    ld a, [H_PDIR]
    cp LEFT
    jr z, .upleft
    cp RIGHT
    jr z, .upright
    ld a, DOWN
    jr .pickedtile
.up
    ld a, [H_PDIR]
    cp LEFT
    jr z, .downleft
    cp RIGHT
    jr z, .downright
    ld a, UP
    jr .pickedtile
.left
    ld a, [H_PDIR]
    cp UP
    jr z, .upright
    cp DOWN
    jr z, .downright
    ld a, LEFT
    jr .pickedtile
.right
    ld a, [H_PDIR]
    cp UP
    jr z, .upleft
    cp DOWN
    jr z, .downleft
    ld a, RIGHT
    jr .pickedtile
.upleft
    ld a, UPLEFT
    jr .pickedtile
.upright
    ld a, UPRIGHT
    jr .pickedtile
.downleft
    ld a, DOWNLEFT
    jr .pickedtile
.downright
    ld a, DOWNRIGHT
    
.pickedtile
    ld d, a
    ld a, [H_PDIR]
    sla a
    sla a
    sla a
    sla a
    add d
    ld [hli], a
    ld a, $ff
    ld [hl], a
    ret

IsInTail: ; sets carry if yes
    ld hl, W_TAIL
.tailloop
    ld a, [hli] ; x
    cp $ff
    jr z, .notail
    cp b
    jr nz, .notx
    ld a, [hli] ; y
    cp c
    jr nz, .noty
    jr .intail

.notx
    inc hl
.noty
    inc hl
    jr .tailloop
.notail
    xor a
    ret
.intail
    scf
    ret

Modulo: ; modulo d
.loop
    cp a, d
    ret c
    sub a, d
    jr .loop

ModuloB:
.loop
    cp a, b
    ret c
    sub a, b
    jr .loop

DivB:
    ld c, 0
.loop
    cp a, b
    jr c, .ret
    sub a, b
    inc c
    jr .loop
.ret
    ld a, c
    ret

MoveFood:
    call GetRNG
    ld d, $14
    call Modulo
    ld b, a
    call GetRNG
    ld d, $12
    call Modulo
    ld c, a
    call IsInTail
    jr c, MoveFood
    ld a, [H_PX]
    cp b
    jr nz, .good
    ld a, [H_PY]
    cp c
    jr z, MoveFood
.good
    ld a, b
    ld [H_FX], a
    ld a, c
    ld [H_FY], a
    xor a
    ld [H_FFRAME], a
    ret

DefaultTail:
    db $0, $9, (RIGHT<<4) + RIGHT
    db $1, $9, (RIGHT<<4) + RIGHT
    db $ff

SnakeInit:
    xor a
    ld [H_SCORE], a
    ld a, 2
    ld [H_PX], a
    ld a, 9
    ld [H_PY], a
    ld a, 3
    ld [H_PDIR], a
    ld [H_PLASTDIR], a
    ld hl, DefaultTail
    ld de, W_TAIL
    call CopyDataFF
    call MoveFood
    jr SnakeLoop

SnakeLoop:
    halt
    ld a, [H_PX]
    ld b, a
    ld a, [H_PY]
    ld c, a
    
    ld a, [H_JOYNEW]
    and a, %00001000
    jr nz, .pause
    
    ld a, [H_JOY]
    ld d, a
    ; down up left right
    and a, %10000000
    jr nz, .down
    ld a, d
    and a, %01000000
    jr nz, .up
    ld a, d
    and a, %00100000
    jr nz, .left
    ld a, d
    and a, %00010000
    jr nz, .right
    jr .done
.down
    ld a, [H_PLASTDIR]
    cp a, UP
    jr z, .done
    ld a, DOWN
    jr .setdir
.up
    ld a, [H_PLASTDIR]
    cp a, DOWN
    jr z, .done
    ld a, UP
    jr .setdir
.left
    ld a, [H_PLASTDIR]
    cp a, RIGHT
    jr z, .done
    ld a, LEFT
    jr .setdir
.right
    ld a, [H_PLASTDIR]
    cp a, LEFT
    jr z, .done
    ld a, RIGHT
    jr .setdir
.pause
    ld a, $1
    ld [H_PAUSE], a
    jr .done
.setdir
    ; prefer other directions
    ; this lets us go diagonally more easily
    ld h, a
    ld a, [H_PLASTDIR]
    cp h
    jr z, .done
    ld a, h
    ld [H_PDIR], a
    jr .done

.done
    ld a, [H_TIMER]
    cp a, $8
    jr nz, SnakeLoop
    
    ld a, [H_PAUSE]
    and a
    jr z, .step
    ; paused
    ld hl, PauseString
    decoord 0,8
    call WriteString
    
    ld hl, ScoreString
    decoord 1,6
    call WriteString
    
    ld a, [H_SCORE]
    call WriteNumber
    
    call WaitForKey

.step
    ; Time to move, then redraw.
    xor a
    ld [H_TIMER], a
    ld [H_PAUSE], a
    
    call AppendTail
    ld a, [H_ATE]
    and a
    jr z, .pop
    xor a
    ld [H_ATE], a
    jr .dontpop
.pop
    call PopTail
.dontpop

    ld a, [H_PX]
    ld b, a
    ld a, [H_PY]
    ld c, a
    ld a, [H_PDIR]
    and a
    jr z, .mdown
    dec a
    jr z, .mup
    dec a
    jr z, .mleft
    jr .mright
.mdown
    inc c
    jr .applymove
.mup
    dec c
    jr .applymove
.mleft
    dec b
    jr .applymove
.mright
    inc b
.applymove
    ld a, b
    ld [H_PX], a
    ld a, c
    ld [H_PY], a
    ld a, [H_PDIR]
    ld [H_PLASTDIR], a
    
    ; check if out of bounds
    ld a, b
    cp $ff
    jr z, .lose
    cp $14
    jr z, .lose
    ld a, c
    cp $ff
    jr z, .lose
    cp $12
    jr z, .lose
    
    ; check if on tail
    call IsInTail
    jr c, .lose
    
    ; check if on food
    ld a, [H_FX]
    cp b
    jr nz, .nofood
    ld a, [H_FY]
    cp c
    jr nz, .nofood
    call MoveFood
    ld a, $1
    ld [H_ATE], a
    ld hl, H_SCORE
    inc [hl]
    
.nofood
    di ; disable interrupts so possible lag won't cause the screen to be drawn blank
    call ClearScreen
    
    call DrawFood
    call DrawTail
    call DrawHead
    ei
    xor a
    jp SnakeLoop
.lose
    call GameOver
    ret

GameOver:
    ld hl, GameOverString
    decoord 16,5
    call WriteString
    
    ld hl, StartAgainString
    decoord 17,1
    call WriteString
    
    ld hl, ScoreString
    decoord 1,6
    call WriteString
    
    ld a, [H_SCORE]
    call WriteNumber
    
    ret

Tiles: INCBIN "gfx/tiles.gb"
TilesEnd

SECTION "bank1",DATA,BANK[$1]
